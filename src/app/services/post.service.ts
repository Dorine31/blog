import { Subject } from 'rxjs';

export class PostService {

  postSubject = new Subject<any[]>();

  private posts = [
     {
       title: 'Structuration de mon application',
       content: 'Avec Angular, la structuration de l\'application se fait par la création d\'une arborescence de components. En plus du component principal, j\'ai créé deux autres components : un post-list-component qui correspond à la liste de mes posts et un post-list-item-component qui constitue chaque post.'
     },
     {
       title: 'Des données dynamiques !',
       content:'Plusieurs possibilités s\'offrent à nous pour utiliser des données de manière dynamique : l\'interpolation, la liaison par propriétés, la liaison par évènements, la liaison à double sens. Sur mes boutons, j\'ai ajouté une liaison par évènement : au click une méthode permet d\'augmenter (ou de diminuer) le nombre de likes. J\'ai aussi utilisé le décorateur @Input afin de faire passer des données (array de posts) d\'un component parent à un component enfant et la liaison par propriété pour passer mon array d\'un component à un autre.'
     },
     {
       title: 'Quel rôle ont les directives ?',
       content: 'Elles modifient la structure du document -> de manière structurelles : *ngIf, *ngFor, etc. >Ou elles modifient le comportement d\'un objet déjà existant -> par attribut : ngStyle, ngClass, etc.\nDans mon blog, j\'ai utilisé *ngFor pour créer un array de mes posts, ngStyle pour modifier le style de l\'affichage de la date. ngClass m\'a permis d\'ajouter une classe qui colorie le post en vert si mes "like" sont positifs ou en rouge si mes "like" sont négatifs. Très pratique !'
     },
     {
       title: 'Utilisation des pipes',
       content:'Ici par exemple, j\'ai utilisé DatePipe pour modifier la date en temps réel, de manière asynchrone et j\'ai modifié le format de la date, directement à l\'intérieur de mon pipe.'
     }
   ]
   emitPostSubject() {
     this.postSubject.next(this.posts.slice());
   }
   
   likeOne(index: number) {
     this.posts[index].loveIts ++;
     this.emitPostSubject;
   }
}
