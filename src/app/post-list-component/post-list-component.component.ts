import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-post-list-component',
  templateUrl: './post-list-component.component.html',
  styleUrls: ['./post-list-component.component.scss']
})
export class PostListComponentComponent implements OnInit {
  @Input() posts: any=[]; // passage de l'array post au component enfant
  //j'ai modifié "Posts" par "any=" suite à la correction d'un de mes collègues !

  @Input() indexOfPost: number;
  loveIts:number = 0;
  constructor() {

  }

  ngOnInit() {
  }
  onAddNumber(): void {
    this.loveIts ++;
  };
  onRemoveNumber(): void {
    this.loveIts --;
  };
  getSuccess (): boolean {
    if(this.loveIts > 0){
        return true;
      }
  };
  getDanger (): boolean {
    if(this.loveIts < 0){
        return true;
      }
  };
  getColor() {
    if(this.loveIts >0) {
      return 'green';
    } else if(this.loveIts<0) {
      return 'red';
    }
  };
}
