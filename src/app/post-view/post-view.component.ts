import { Component, OnInit } from '@angular/core';

import { Subscription } from 'rxjs/Subscription';
@Component({
  selector: 'app-post-view',
  templateUrl: './post-view.component.html',
  styleUrls: ['./post-view.component.scss']
})
export class PostViewComponent implements OnInit {

  postSubscription : Subscription;

  constructor() { }

  ngOnInit() {
    this.postSubscription = this.postService.postSubject.Subscribe(
      (posts:any[])=>{
        this.posts = posts;
      }
    );
    this.postService.emitPostSubject();
  }

}
