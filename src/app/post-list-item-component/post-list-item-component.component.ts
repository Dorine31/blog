import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-post-list-item-component',
  templateUrl: './post-list-item-component.component.html',
  styleUrls: ['./post-list-item-component.component.scss']
})
export class PostListItemComponentComponent implements OnInit {
  @Input() post:any[];

  created_at:Promise<Date>;

constructor  () {
  this.created_at = new Promise((resolve, reject)=> {
    const date = new Date();
    setTimeout(
      ()=> {
        resolve(date);
      }, 1000
    );
  });
 }

  ngOnInit() { }

}
