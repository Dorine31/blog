import { Component, Input, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { PostService } from './services/post.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {
  title1 = 'Simples posts avec Angular';

  //array of posts
  posts: any[];

  ngOnInit() {
    this.posts = this.postService.posts;
  }

  constructor(private postService: PostService) {

  }
}
